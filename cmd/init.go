/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"errors"

	"github.com/Masterminds/semver"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var version, tag, context string

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "initalizes this dockerbuilder instance",
	Long:  `Intializes a config for dockerbuilder to know the current version and tag`,
	Run: func(cmd *cobra.Command, args []string) {

		if version == "" {
			if viper.GetString("version") == "" {
				viper.Set("version", "0.0.1")
			}
			version = viper.GetString("version")
		}

		if tag == "" {
			if viper.GetString("tag") == "" {
				cmd.PrintErr(errors.New("tag was not set"))
				return
			}
			tag = viper.GetString("tag")
		}

		if context == "" {
			if viper.GetString("context") == "" {
				viper.Set("context", ".")
			}
			context = viper.GetString("context")
		}

		//fmt.Println("init called")
		_, err := semver.NewVersion(version)

		if err != nil {
			cmd.PrintErr(err)
			return
		}

		viper.Set("tag", tag)
		viper.Set("version", version)
		viper.Set("context", context)

		err = viper.WriteConfigAs("./.dockerbuilder.yaml")

		if err != nil {
			cmd.PrintErr(err)
			return
		}

		cmd.Println("Sucessfully initalized!")
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
	initCmd.Flags().StringVarP(&version, "version", "v", "", "version to set this to")
	initCmd.Flags().StringVarP(&tag, "tag", "t", "", "sets the tag")
	initCmd.Flags().StringVarP(&context, "context", "c", ".", "sets the context")

	// viper.BindPFlag("version", initCmd.Flags().Lookup("version"))
	// viper.BindPFlag("package", initCmd.Flags().Lookup("package"))

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// initCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// initCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
