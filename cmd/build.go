/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/Masterminds/semver"
	"gitlab.com/readyroster/dockerbuilder/actions"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const versionfile = "./dockerbuildversion"

var shouldBuild, shouldPush bool

var bumpString string

var packageName string

// buildCmd represents the build command
var buildCmd = &cobra.Command{
	Use:   "build",
	Short: "Builds the docker container optionally incrementing versions",
	Long:  `Builds a docker image of either the current or provided directory, using the tag last set or incremented based on flags`,
	Run: func(cmd *cobra.Command, args []string) {
		if viper.GetString("version") == "" || viper.GetString("tag") == "" || viper.GetString("context") == "" {
			fmt.Println("Please run init before trying to proceeding with this operation")
			return
		}

		// verStr := viper.GetString("version")

		// if verStr != "" {
		// 	ver, err := semver.NewVersion(verStr)
		// 	if err != nil {
		// 		panic(err)
		// 	}
		// 	version.WriteVersion(versionfile, ver)
		// }

		ver, err := semver.NewVersion(viper.GetString("version"))

		if err != nil {
			panic(err)
		}

		build := actions.NewBuildAction(viper.GetString("tag"), ver, viper.GetString("context"))

		inc := actions.IncrementVersionFromString(bumpString)

		build.IncrementVersion(inc)

		viper.Set("version", build.Version.Original())

		err = viper.WriteConfigAs("./.dockerbuilder.yaml")
		if err != nil {
			panic(err)
		}

		cmd.Printf("Version sucessfully bumped to: %s \n", viper.GetString("version"))

		if shouldBuild {
			err := build.BuildDockerImage()
			if err != nil {
				cmd.PrintErr(err)
				return
			}
			fmt.Println("Built docker image with tag: " + build.GetDockerTag())
		}

		if shouldPush {
			err = build.PushDockerImage()
			if err != nil {
				cmd.PrintErr(err)
				return
			}
			fmt.Println("Pushed docker image with tag: " + build.GetDockerTag())
		}
	},
}

func init() {
	rootCmd.AddCommand(buildCmd)

	buildCmd.Flags().StringVarP(&bumpString, "version", "v", "none", "Bumps the version can be major,minor,patch or none defaults to none")

	buildCmd.Flags().BoolVarP(&shouldBuild, "build", "b", false, "")
	buildCmd.Flags().BoolVarP(&shouldPush, "push", "p", false, "")
}

func generateTag() string {
	return fmt.Sprint(viper.GetString("package"), ":", viper.GetString("version"))
}
