package actions

import (
	"fmt"
	"strings"

	"github.com/Masterminds/semver"
	"gitlab.com/readyroster/dockerbuilder/docker"
)

//IncrementVersion is used to select what version to increment if any
type IncrementVersion int

const (
	//IVNone means do not change the version
	IVNone IncrementVersion = iota
	//IVMajor will increment the major
	IVMajor
	//IVMinor will increment the minor
	IVMinor
	//IVPatch will increment the patch
	IVPatch
)

//IncrementVersionFromString gets a incrementversion from a string, defaults to IVNone
func IncrementVersionFromString(str string) IncrementVersion {
	switch strings.ToLower(str) {
	case "major":
		return IVMajor
	case "minor":
		return IVMinor
	case "patch":
		return IVPatch
	}
	return IVNone
}

//BuildAction is used to build a docker project
type BuildAction struct {
	Version    *semver.Version
	PackageTag string
	Context    string
}

//NewBuildAction creates a new build action
func NewBuildAction(packageTag string, version *semver.Version, context string) *BuildAction {
	return &BuildAction{Version: version, PackageTag: packageTag, Context: context}
}

//IncrementVersion increses the version based on the version
func (build *BuildAction) IncrementVersion(version IncrementVersion) {
	if version == IVMajor {
		version := build.Version.IncMajor()
		build.Version = &version
	}
	if version == IVMinor {
		version := build.Version.IncMinor()
		build.Version = &version
	}
	if version == IVPatch {
		version := build.Version.IncPatch()
		build.Version = &version
	}
}

//PushDockerImage pushes the docker image based on current generated tag(BuildDockerImage should be called first!)
func (build *BuildAction) PushDockerImage() error {
	return docker.PushImage(build.GetDockerTag())
}

//BuildDockerImage builds a docker image based on the current parmamters
func (build *BuildAction) BuildDockerImage() error {
	return docker.BuildImage(build.Context, build.GetDockerTag())
}

//GetDockerTag generates a docker tag based on the current instance
func (build *BuildAction) GetDockerTag() string {
	return fmt.Sprint(build.PackageTag, ":", build.Version.String())
}
