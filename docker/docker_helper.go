package docker

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/docker/docker/client"
)

const dockerVersion = "v1.24"

var dockerClient *client.Client

func init() {
	//defaultHeaders := map[string]string{"Content-Type": "application/tar"}
	//var err error
	//handle diffrences in opening docker connect with windows and linux
	// if runtime.GOOS == "windows" {
	// 	dockerClient, err = client.NewClient("npipe:////./pipe/docker_engine", dockerVersion, nil, defaultHeaders)
	// }
	// if runtime.GOOS == "linux" {
	// 	dockerClient, err = client.NewClient("unix:///var/run/docker.sock", dockerVersion, nil, defaultHeaders)
	// }

	// dockerClient, err = client.NewEnvClient()
	// if err != nil {
	// 	fmt.Println("Error on creations")
	// 	panic(err)
	// }
}

//PushImage pushes the docker image
func PushImage(tag string) error {
	cmd := exec.Command("docker", "push", tag)

	//stdout, err := cmd.Output()
	out, err := cmd.StdoutPipe()
	defer out.Close()

	cmd.Start()

	scanner := bufio.NewScanner(out)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	// buf := make([]byte, 8)
	// for {

	// 	n, err := out.Read(buf)
	// 	if err == io.EOF {
	// 		break
	// 	}
	// }

	// if err != nil {
	// 	fmt.Print(err)
	// 	fmt.Println(cmd.Args)
	// 	//return err
	// }

	//fmt.Print(string(stdout))
	return err
}

//BuildImage builds a docker image in the pkgDir with the tag provided
func BuildImage(pkgDir, tag string) error {
	cmd := exec.Command("docker", "build", "-t", tag, pkgDir)

	out, err := cmd.StdoutPipe()
	defer out.Close()

	cmd.Start()

	scanner := bufio.NewScanner(out)

	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	// stdout, err := cmd.Output()
	// //stdout, err := cmd.Output()

	// if err != nil {
	// 	fmt.Print(err)
	// 	fmt.Println(cmd.Args)
	// 	//return err
	// }

	// fmt.Print(string(stdout))

	return err
}

func createTarballDir(tarballFilePath string, filePath string) error {

	array := []string{}
	filepath.Walk(filePath, func(root string, info os.FileInfo, err error) error {
		if !info.IsDir() {
			rootLocal := strings.ReplaceAll(root, "", "")
			fmt.Println(rootLocal)
			array = append(array, rootLocal)
		}

		return nil
	})

	return createTarball(tarballFilePath, array)
}

func createTarball(tarballFilePath string, filePaths []string) error {
	file, err := os.Create(tarballFilePath)
	if err != nil {
		return fmt.Errorf("Could not create tarball file '%s', got error '%s'", tarballFilePath, err.Error())
	}
	defer file.Close()

	gzipWriter := gzip.NewWriter(file)
	defer gzipWriter.Close()

	tarWriter := tar.NewWriter(gzipWriter)
	defer tarWriter.Close()

	for _, filePath := range filePaths {
		err := addFileToTarWriter(filePath, tarWriter)
		if err != nil {
			return fmt.Errorf("Could not add file '%s', to tarball, got error '%s'", filePath, err.Error())
		}
	}

	return nil
}

// Private methods

func addFileToTarWriter(filePath string, tarWriter *tar.Writer) error {
	file, err := os.Open(filePath)
	if err != nil {
		return fmt.Errorf("Could not open file '%s', got error '%s'", filePath, err.Error())
	}
	defer file.Close()

	stat, err := file.Stat()
	if err != nil {
		return fmt.Errorf("Could not get stat for file '%s', got error '%s'", filePath, err.Error())
	}

	header := &tar.Header{
		Name:    filePath,
		Size:    stat.Size(),
		Mode:    int64(stat.Mode()),
		ModTime: stat.ModTime(),
	}

	err = tarWriter.WriteHeader(header)
	if err != nil {
		return fmt.Errorf("Could not write header for file '%s', got error '%s'", filePath, err.Error())
	}

	_, err = io.Copy(tarWriter, file)
	if err != nil {
		return fmt.Errorf("Could not copy the file '%s' data to the tarball, got error '%s'", filePath, err.Error())
	}

	return nil
}
