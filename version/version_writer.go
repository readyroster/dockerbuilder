package version

import (
	"io/ioutil"
	"os"

	"github.com/Masterminds/semver"
)

//ReadVersion reads the version from the path and returns it
func ReadVersion(path string) (*semver.Version, error) {
	data, err := ioutil.ReadFile("./dockerbuilderversion")
	if err != nil {
		return nil, err
	}
	version, err := semver.NewVersion(string(data))
	return version, err
}

//WriteVersion writes to a file or creates it if it doesnt exist
func WriteVersion(path string, version *semver.Version) error {
	file, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR, 777)
	defer file.Close()
	if err != nil {
		return err
	}

	file.WriteString(version.String())
	return nil
}
